#pragma once
#include "PurrmaidRenderer.hpp"
namespace Purrmaid::renderer::sfml
		
{

	enum class WindowType
	{
		Unknown = -1,
		Text = 1,
		Default  = Text
	};
	class Window
	{
	public:
		Window(sf::IntRect, std::string);

		sf::Sprite draw(sf::RenderWindow &);
		void SetBoarderThickness(int);


		//~Window();

	private:
		sf::IntRect WindowRect;
		std::string WindowName;
		sf::Font font;


		WindowType Type;
		float Boarder_Thiccness;
		int Title_Thiccness;

		//sf::RectangleShape Body_Shape;
		sf::Texture * WindowTexture;
		sf::RenderTexture RT;
		sf::FloatRect Pos;

		sf::RectangleShape draw_window_area();
		sf::RectangleShape draw_title_bar();
		sf::Text draw_title_text();
	};

				
				
}
