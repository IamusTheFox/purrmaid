#pragma once
#include <vector>
#include <functional>
#include <future>
#include <iostream>
#include <type_traits>
#include "PurmaidPrint.hpp"



namespace Purrmaid {


	template <typename R, typename... A>
	class PurrmaidDelegate {

		std::vector<std::function<R(A...)>> Function_Vector;

	public:
		void operator+=(std::function<R(A...)> a) { Function_Vector.push_back(a); }

		auto operator()(A... a) {
			if constexpr (std::is_void<R>::value != true)
			{
				R r{};
				for (std::function<R(A...)>& funs : Function_Vector) {
					r = funs(a...); //std::async(funs, a...);
					
			
				}
				return r;
			}
			else
				for (std::function<R(A...)>& funs : Function_Vector) {
					funs(a...); //std::async(funs, a...);
				}
		}

		size_t GetSize() { return Function_Vector.size(); }
	};
}