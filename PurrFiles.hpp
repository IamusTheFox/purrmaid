#pragma once

class PurrFiles
{
public:
	PurrFiles();
	~PurrFiles();

	template <typename LoadType, typename LoadFunction>
	LoadType LoadFile(LoadFunction);

	bool IsValidLoc();

private:

};

PurrFiles::PurrFiles()
{
}

PurrFiles::~PurrFiles()
{
}