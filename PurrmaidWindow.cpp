#include "PurrmaidWindow.hpp"
namespace Purrmaid::renderer::sfml
{
	Window::Window(sf::IntRect Size, std::string Title)
	{
		WindowTexture = nullptr;

		if (!font.loadFromFile("OpenDyslexic3-Regular.ttf"))
		{
			// error...
		}

		WindowRect = Size;
		WindowName = Title;

		Boarder_Thiccness = 1;
		Title_Thiccness = 32;

		Pos.height = Size.height;
		Pos.width = Size.width;
		Pos.top = Size.top;
		Pos.left = Size.left;

		Type = WindowType::Default;


		if (!RT.create(Size.height + (2 * Boarder_Thiccness), Size.width + (2 * Boarder_Thiccness)))//(Size.height + (Boarder_Thiccness  4), Size.width + (Boarder_Thiccness * 4)))
		{
			// error...
		}


	}

	sf::RectangleShape Window::draw_window_area()
	{
		sf::RectangleShape Body_Shape;
		Body_Shape.setSize(sf::Vector2f((float)Pos.width, (float)Pos.height));

		if (Boarder_Thiccness != 0)
			Body_Shape.setPosition(sf::Vector2f((Boarder_Thiccness), (Boarder_Thiccness)));


		//Body_Shape.setPosition(sf::Vector2f((int)Size.top, (int)Size.left));
		Body_Shape.setFillColor(sf::Color::Blue);
		Body_Shape.setOutlineThickness(Boarder_Thiccness);


		return Body_Shape;
	}

	sf::RectangleShape Window::draw_title_bar()
	{
		sf::RectangleShape Body_Shape;
		Body_Shape.setSize(sf::Vector2f(Pos.width, Title_Thiccness));

		if (Boarder_Thiccness != 0)
			Body_Shape.setPosition(sf::Vector2f((Boarder_Thiccness), (Boarder_Thiccness)));


		//Body_Shape.setPosition(sf::Vector2f((int)Size.top, (int)Size.left));
		Body_Shape.setFillColor(sf::Color::Cyan);
		Body_Shape.setOutlineThickness(Boarder_Thiccness);


		return Body_Shape;
	}

	sf::Text Window::draw_title_text()
	{

		sf::Text RV;
		RV.setFont(font);
		RV.setString(WindowName);
		RV.setCharacterSize(20);
		RV.setFillColor(sf::Color::Magenta);
		return RV;
	}




	//sf::RectangleShape Window::GetRectangleShape() { return Body_Shape; }

	void Window::SetBoarderThickness(int n_thiccness) { Boarder_Thiccness = n_thiccness; }


	sf::Sprite Window::draw(sf::RenderWindow & window)
	{

		//if (!RT.create(Pos.height + (2 * Boarder_Thiccness), Pos.width + (2 * Boarder_Thiccness)))//(Size.height + (Boarder_Thiccness  4), Size.width + (Boarder_Thiccness * 4)))
		if (!RT.create(Pos.width + (2 * Boarder_Thiccness), Pos.height + (2 * Boarder_Thiccness)))//(Size.height + (Boarder_Thiccness  4), Size.width + (Boarder_Thiccness * 4)))
		{
			// error...
		}

		RT.clear();

		RT.draw(draw_window_area());
		RT.draw(draw_title_bar());
		RT.draw(draw_title_text());
		RT.display();

		sf::Sprite sprite(RT.getTexture());
		sprite.setPosition(Pos.top, Pos.left);

		//window.draw(spirte);
		return sprite;
	}
}
