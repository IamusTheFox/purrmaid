
#ifdef _Win32
#define _CRTDBG_MAP_ALLOC

#include <stdlib.h>
#include <crtdbg.h>
#endif

#include <vector>
#include <iostream>
#include <thread>
#include <mutex>
#include <string>

#include <functional>


//#include <SDL.h>
//#include <boost\units\unit.hpp>
//#include <boost\thread\thread.hpp>
std::mutex thing;
#include "Purrmaid.hpp"
#include "Base_Object.hpp"

//#include <lua53/lua.hpp>

//#include "Purrmaid.hpp"




int QuitProgram() {
	std::cout << "exiting\n";
	exit(0);
}
extern "C" int lua_quit(lua_State* state) {
	// The number of function arguments will be on top of the stack.
	int args = lua_gettop(state);


	// Push the return value on top of the stack. NOTE: We haven't popped the
	// input arguments to our function. To be honest, I haven't checked if we
	// must, but at least in stack machines like the JVM, the stack will be
	// cleaned between each function call.

	lua_pushnumber(state, 123);

	exit(0);


	// Let Lua know how many return values we've passed
	return 0;
}


class GameThreadCls : public Purrmaid::Threading::IThread_Runnable {

	std::shared_ptr<std::thread> local_thread;

	auto add_base_object(std::string s, Purrmaid::renderer::sfml::manager& tm) {
		//std::shared_ptr<Purrmaid::renderer::sfml::enitity> &foo;
		std::shared_ptr<Base_Object> bar;
		bar = std::make_shared<Base_Object>("Swiftys_Test_Sprite.png");

		auto foo = std::dynamic_pointer_cast<Purrmaid::renderer::sfml::enitity>(bar);

		tm.add_enitity(foo);

		return bar;
	}

	void do_the_things() {
		Purrmaid::renderer::sfml::manager tm;


		tm.add_enitity("swifty3.png");

		std::array<std::shared_ptr<Base_Object>, 0xff> Bos;

		for (auto & i : Bos)
		{
			i = add_base_object("Swiftys_Test_Sprite.png", tm);
			i->sprite.setOrigin(32, 64);
			i->sprite.setPosition(256.0f, 256.0f);
			i->sprite.setPosition(256.0f, 256.0f);
		}
		
		tm.AddQuit(QuitProgram);
		
		while (tm.isOpen()) {

			//auto Framerate = 1.f / tm.//getFrameTime();
			//Purrmaid::Scripting::Lua::Manager Lua_Script("test3.lua");
			if (tm.GetFrameTime() >= 0.0600f) {
				//std::cout << tm.GetFrameTime() << '\n';
				tm.do_render();
				for (auto& i : Bos)
					i->do_ai();
				
				//bar1->do_ai();
				//bar2->do_ai();
				//bar3->do_ai();
				//bar4->do_ai();

				//Game::do_ai(bar2);
				//Game::do_ai(bar);
				tm.ResetClock();
			}
			tm.do_events();

			//Lua_Script.run_script();
		}
	}

public:
	// Inherited via IThread_Runnable


	void Start() override {
		std::cout << "GameThreadCls::Start()\n";
		local_thread = std::make_shared<std::thread>(&GameThreadCls::do_the_things, this);
		local_thread->detach();
	}
	virtual void Pause() override {
	}
	virtual bool hasPaused() override {
		return false;
	}
	virtual bool hasDied() override {
		return false;
	}
	virtual void Join() override {
	}
};



extern "C" int howdy(lua_State* state) {
	// The number of function arguments will be on top of the stack.
	int args = lua_gettop(state);

	printf("howdy() was called with %d arguments:\n", args);

	for (int n = 1; n <= args; ++n) {
		printf("  argument %d: '%s'\n", n, lua_tostring(state, n));
	}



	// Push the return value on top of the stack. NOTE: We haven't popped the
	// input arguments to our function. To be honest, I haven't checked if we
	// must, but at least in stack machines like the JVM, the stack will be
	// cleaned between each function call.

	lua_pushnumber(state, 123);

	// Let Lua know how many return values we've passed
	return 1;
}
extern "C" int Lua_Run_Script(lua_State* state) {
	// The number of function arguments will be on top of the stack.
	int args = lua_gettop(state);

	Purrmaid::Scripting::Lua::Manager SM(lua_tostring(state, 1));
	std::cout << lua_tostring(state, 1) << '\n';
	SM.add_script_funtion("howdy", howdy);
	SM.add_script_funtion("purrmaid_quit", lua_quit);
	SM.add_script_funtion("purrmaid_run_script", Lua_Run_Script);

	SM.run_script();
	// Let Lua know how many return values we've passed
	return 0;
}
int main(int argc, char* argv[]) {
	//PyTestsThings(argc, argv);
	std::shared_ptr<Purrmaid::Threading::IThread_Runnable> MyThing(new GameThreadCls);

	{
		Purrmaid::Scripting::Lua::Manager SM("test1.lua");
		SM.add_script_funtion("howdy", howdy);
		SM.add_script_funtion("purrmaid_quit", lua_quit);

		SM.run_script();
	}


	Purrmaid::Threading::Manager TM;
	TM.Add(MyThing);
	{
		Purrmaid::Scripting::Lua::Manager SM("test2.lua");
		SM.add_script_funtion("howdy", howdy);

		SM.run_script();
	}
	for (;;)
		;
	return 0;
}