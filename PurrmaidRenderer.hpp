#include "renderer_data.h"
#include "PurrmaidDelegate.hpp"
#include "ThreadManager.hpp"
#include <string>
#pragma once

#ifdef SFML
	#include <SFML/Main.hpp>
	#include <SFML/System.hpp>
	#include <SFML/Window.hpp>
	#include <SFML/Graphics.hpp>
#endif
namespace Purrmaid::renderer::sfml
{
	class Window;
	struct enitity
	{
		sf::Texture texture;
		sf::Sprite sprite;

		virtual bool load(std::string str);
		enitity(std::string str); // init sprit, and texture



	};
	class manager
	{
		std::shared_ptr<sf::RenderWindow> window;
		std::vector<std::shared_ptr<enitity>> Enities_Vectors;
		std::vector < std::shared_ptr<Window>> Windows_Vector;

		PurrmaidDelegate<int> Closed;
		//PurrmaidDelegate<int, sf::Event, enitity> Movement;
		PurrmaidDelegate<int, sf::Event> SFML_events;
		sf::Clock clock;



	public:
		manager();
		void do_events();
		void do_render();

		std::shared_ptr<enitity> add_enitity(std::string);
		std::shared_ptr<enitity> add_enitity(std::shared_ptr<enitity>&);
		std::shared_ptr<enitity> add_enitity(enitity);

		std::shared_ptr<Window> add_Window(sf::IntRect, std::string);

		void AddQuit(const std::function<int()> &nQuit);
		void AddEvents(const std::function<int(sf::Event)> &nEvent);

		float GetFrameTime();
		float ResetClock();

		bool isOpen();

	};

}


