#pragma once
#include <string>

#include "PurrmaidDelegate.hpp"


int PyTestsThings(int argc, char**argv);

namespace Purrmaid
{
	class ScriptManager
	{
		PurrmaidDelegate<int, std::string> ScriptDelegate;
	public:
		ScriptManager(int arg_count, char** arg_values);
		void RunScript(std::string);
		void RunScriptFromString(std::string);

		
		
	};

}