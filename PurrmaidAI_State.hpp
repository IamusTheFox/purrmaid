#pragma once
#include <array>
#include <type_traits>

namespace Purrmaid {
	template <class T_Callable, class T_Symbol>
	struct State_Pair {
		T_Callable callable;
		T_Symbol symbol;
	};

	template <class T_State_Data, std::size_t Array_Size>
	class State_CLS {
	public:
		std::array<T_State_Data, Array_Size> _array;

		template <class T_Symbol, class... T_Args>
		constexpr auto run(const T_Symbol& sm, T_Args&... targs) {

				for (auto& i : _array)
					if (i.symbol == sm) {
						return i.callable(targs...);
					}
		}
		constexpr auto add_at(int index, T_State_Data& data) {
			auto rv{ false };
			if (index >= Array_Size)
				rv = false;
			else {
				_array[index] = data;
				rv = true;
			}

			return rv;
		}
		template <class T_Symbol>
		constexpr bool find(T_Symbol sm) {
			for (auto& i : _array)
				if (i.symbol == sm) {
					return true;
				}
			return false;
			
		}

		constexpr auto get_size() { return Array_Size; }
	};
}