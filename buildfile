libs = sfml-audio%lib{sfml-audio}
libs += sfml-network%lib{sfml-network}
libs += sfml-window%lib{sfml-window}
libs += sfml-graphics%lib{sfml-graphics}
libs += sfml-system%lib{sfml-system}
libs += liblua%lib{lua}
libs += pthread%lib{pthread}

exe{purrmaid}: {cxx hxx ixx txx}{**} $libs

