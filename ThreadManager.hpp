#pragma once
#include <thread>
#include <vector>
#include <deque>
#include <memory>
#include <functional>
#include <string>
//#include "Purrmaid.hpp"
#include <any>
//#include "PurrmaidTypes.hpp"

namespace Purrmaid 
{
	namespace Threading
	{
		struct IThread_Runnable
		{
			virtual void Start();
			virtual void Pause();
			virtual bool hasPaused();
			virtual bool hasDied();
			virtual void Join();
			//std::shared_ptr<std::thread> GetThread();
		};

		class Thread_Runnable : public IThread_Runnable
		{
			void Start() override;
			void Pause() override;
			bool hasPaused() override;
			bool hasDied() override;
			void Join() override;
			//std::shared_ptr<std::thread> GetThread();
		};

		struct RunnableItem
		{
			std::shared_ptr<IThread_Runnable> Runnable_Thread;
			int ID;
		};

		class Manager
		{
			std::vector<std::shared_ptr<IThread_Runnable>> Threads;
		public:

			//void Add();
			//void Add(const std::shared_ptr<RunnableItem> &);
			void Add(std::shared_ptr<IThread_Runnable> & Thread);

			void PauseAll();
			
			void JoinAll();

		};
	}
	
	typedef std::function<void(void)> ThreadManagerFunction;

	struct ThreadItem
	{
		std::string ThreadName;
		std::unique_ptr<bool> HasTerminated;
		std::thread ThreadPtr;
		ThreadManagerFunction Function;
		std::shared_ptr<bool> PauseThisThread;

		//std::shared_ptr<PurrmaidType> Args;

		int ID;
		//std::shared_ptr<bool> AllTreahPause;


		ThreadItem();
	};

	class ThreadManager
	{

		static ThreadManager ActiveThreadManager;
		std::deque < std::shared_ptr< ThreadItem>> ThreadDeque;

		//std::deque < std::shared_ptr<ThreadItem> ThreadDeque;
	public:
		ThreadManager();
		~ThreadManager();

		//void AddThreadItem();
		void AddThreadItem(const std::shared_ptr<ThreadItem> &Item);

		void PauseID(int);

		
	};
}

