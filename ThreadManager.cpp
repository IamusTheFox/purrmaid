#ifdef _Win32
	#define _CRTDBG_MAP_ALLOC  
	#include <stdlib.h>  
	#include <crtdbg.h>  
#endif
#include "ThreadManager.hpp"


#include <iostream>

namespace Purrmaid
{

	namespace Threading
	{
		
		void IThread_Runnable::Start(){;}
		void IThread_Runnable::Pause(){;}
		bool IThread_Runnable::hasPaused(){return false;}
		bool IThread_Runnable::hasDied(){return false;}
		void IThread_Runnable::Join(){;}
		
		
		void Manager::Add(std::shared_ptr<IThread_Runnable> & Thread)
		{
			Threads.push_back(Thread);

			Threads.back()->Start();
		}


		/*void Manager::AddThread(IThread_Runnable &new_threaditem)
		{
			
		}*/

		
		void Manager::PauseAll()
		{
			for (auto x : Threads)
				x->Pause();
		}
		void Manager::JoinAll()
		{
			for (auto x : Threads)
			{

				x->Join();
			}
		}

		
	}
	ThreadItem::ThreadItem() : ThreadName{ "default" }, ID{ 0 }, HasTerminated{ new bool }, PauseThisThread{ new bool }{}

	ThreadManager::ThreadManager()
	{
	}


	ThreadManager::~ThreadManager()
	{
		std::cout << "asdasdfsdaf";
		while (ThreadDeque.size())
		{
			(ThreadDeque[0])->ThreadPtr.join();
			ThreadDeque.pop_front();
		}
	}


	
	void ThreadManager::AddThreadItem(const std::shared_ptr<ThreadItem> &Item)
	{
		
		Item->ID = ThreadDeque.size();
		Item->ThreadPtr = std::thread(Item->Function);
		ThreadDeque.push_back(Item);
	}

	void ThreadManager::PauseID(int nID)
	{
		for (std::size_t i = 0; i < ThreadDeque.size(); ++i)
		{
			if (ThreadDeque[i]->ID == nID)
			{
				*ThreadDeque[i]->PauseThisThread = true;
			}
		}
	}




	
}