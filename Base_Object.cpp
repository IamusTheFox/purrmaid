#include "Base_Object.hpp"
#ifdef SFML
#include <SFML/Main.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#endif
#include <random>
#include <chrono>

constexpr auto Idle_State = 0, 
Decide_to_Move_State = 1, 
Moving_To_Target_State = 2, 
At_Moving_Target_State = 3, 
Default_State_State = 4, 
Unknown_State_State = 5;

Base_Object::Base_Object(std::string x) : enitity(x){
	load(x);
	sprite.setTexture(texture);
	std::cout << "Base_Object";
	std::cout << "a" << texture.getNativeHandle() << '\n';
	TargetLocation = sf::Vector2f(0.0f, 0.0f);

	_current_state = Idle_State;
	{
		Purrmaid::State_Pair<decltype(Base_Object_State::Default_State)*, int> bo_state_idle{Base_Object_State::Idle, Idle_State};
		
		decltype(bo_state_idle) bo_state_decide_to_move{Base_Object_State::Decide_to_Move, Decide_to_Move_State}; 
		decltype(bo_state_idle) bo_state_moving_to_target{Base_Object_State::Moving_To_Target, Moving_To_Target_State};
		decltype(bo_state_idle) bo_state_at_moving_target{Base_Object_State::At_Moving_Target, At_Moving_Target_State};
		decltype(bo_state_idle) bo_state_default {Base_Object_State::Default_State, Default_State_State};
		decltype(bo_state_idle) bo_state_unknown {Base_Object_State::Unknown_State, Unknown_State_State};
		
		_State.add_at(0, bo_state_idle);
		_State.add_at(1, bo_state_decide_to_move);
		_State.add_at(2, bo_state_moving_to_target);
		_State.add_at(3, bo_state_at_moving_target);
		_State.add_at(4, bo_state_default);
		_State.add_at(5, bo_state_unknown);
	}
}

int Base_Object::getState() {
	return _current_state;
}

void Base_Object::setState(const int& n_state) {
	_current_state = n_state;
}





sf::Vector2f Base_Object::getTargetLocation() { return TargetLocation; }
void Base_Object::setTargetLocation(float x, float y) { TargetLocation.x = x, TargetLocation.y = y; }

void Base_Object::do_ai() {
	if (!_State.find(_current_state))
		Base_Object_State::Unknown_State(*this);
	auto x = _State.run(_current_state, *this);


}

auto constexpr set_speed(const float& c_loc, const float& t_loc, const float& t_spd) {
	auto rv = t_spd;
	auto math = (c_loc - t_loc < 0) ? -(c_loc - t_loc) : (c_loc - t_loc);

	if (t_spd > math)
		rv = math;
	return rv;
}

float constexpr set_move_distance(const float& current_loc, const float& target_loc, const float& target_speed) {
	float rv = set_speed(current_loc, target_loc, target_speed);

	//if above or left
	if (current_loc > target_loc) {
		//rv = set_speed(current_loc, target_loc, target_speed);
		rv = -rv;
	}

	//if below or right
	else if (current_loc < target_loc) {
		//rv = rv;
	}
	// if at target
	else if (current_loc == target_loc) {
		rv = 0.0f;
	}

	return rv;
}
template <class T>
T do_random_think(T max) {
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed);
	//std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, max);
	return distribution(generator);
}

template <class T>
T generate_x_or_y() {
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, 512);
	return distribution(generator);
}



namespace Base_Object_State {
	int Idle(Base_Object& BO) {
		if (do_random_think(0xff) == 0xf) {
			BO.setState(Decide_to_Move_State);
		}
		return 0;
	}
	int Decide_to_Move(Base_Object& BO) {
		BO.setTargetLocation(do_random_think(512), do_random_think(512));
		BO.setState(Moving_To_Target_State);


		return 0;
	}
	int Moving_To_Target(Base_Object& BO) 
	{
		auto move_distance = 15;
		auto pos_x = set_move_distance(BO.sprite.getPosition().x, BO.getTargetLocation().x, move_distance);
		auto pos_y = set_move_distance(BO.sprite.getPosition().y, BO.getTargetLocation().y, move_distance);
		if (pos_x == pos_y)
			if (pos_x == 0.0f) {
				BO.setState(At_Moving_Target_State);
			}
			else {
				pos_x /= 2;
				pos_y = pos_x;
			}
		BO.sprite.move(pos_x, pos_y);
		return 0;
	}
	int At_Moving_Target(Base_Object& BO)
	{
		BO.setState(Idle_State);
		return 0;
	}
	int Default_State(Base_Object& BO) {
		Idle(BO);
		return 0;
	}
	int Unknown_State(Base_Object& BO) {
		BO.setState(Idle_State);
		Default_State(BO);
		return 0;
	}
}
