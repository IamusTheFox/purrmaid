
#include <string>
#include <iostream>
#include "PurmaidPrint.hpp"
#include <future>
#include <deque>
#include <mutex>
//#include <boost/asio.hpp>

namespace Purrmaid {


	std::deque<std::string> Print_List;



	void Do_print() {
		std::mutex locker;

		while (Print_List.size() > 0) {

			std::cout << Print_List.front();
			locker.lock();
			Print_List.pop_front();
			locker.unlock();
		}
		return;
	}

	void PurrPrint(std::initializer_list<std::string> T) {
		for (auto V : T)
			Print_List.push_back(V);

		auto waiting = std::async(std::launch::async, Do_print);
		return;
	}

	void PurrPrintln(std::initializer_list<std::string> T) {
		Print_List.push_back("\n");
		PurrPrint(T);

		return;
	}


	void PurrPrint(std::string V) {
		PurrPrint({ V });
	}


	void PurrPrintln(const char* sun) {
		PurrPrint({ sun, "\n" });
	}
}
