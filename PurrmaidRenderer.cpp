//#include "PurrEventManager.hpp"
#include "PurrmaidWindow.hpp"
namespace Purrmaid::renderer::sfml
{

	bool enitity::load(std::string str)
	{
		std::cout << "enitity::load(" << str << ")\n";
		bool rv = texture.loadFromFile(str.c_str());
		if(!rv)
			std::cout << "test fail load\n";
		return rv;
	}
	enitity::enitity(std::string str)
	{

		std::cout << "enitity::enitity()\n";
		bool load_status = load(str);
		if (!load_status)
			std::cout << "failed to load sprite!\n";
		sprite.setTexture(texture);
	}

	manager::manager()
	{
		window.reset(new sf::RenderWindow(sf::VideoMode(512, 512), "Purr~"));
		window->setFramerateLimit(60);
	}
	void manager::do_events()
	{
		if (window != nullptr)
		{
			if (!window->isOpen())std::cout << "Windows is closed!\n";
			//std::cout << "This is a thing!\n";
			sf::Event event;
			while (window->pollEvent(event))
			{
				SFML_events(event);
				//std::cout << event.type << '\n';
				switch (event.type)
				{
				case sf::Event::Closed:
					Closed();
					window->close();
					std::cout << "debug!\n";

				default:
					break;
				}
			}


		}
		else std::cout << "Target Window null!\n";
	}
	bool manager::isOpen() { return this->window->isOpen(); }

	void manager::do_render()
	{
		window->clear();
		for (auto x : Enities_Vectors)
		{
			//x->sprite.setScale(sf::Vector2f(0.45f, 0.45f));
						
			window->draw(x->sprite);
		}

		for (auto x : Windows_Vector)
		{
			//x->draw(*window);

			window->draw(x->draw(*window));
		}


		window->display();
	}

	std::shared_ptr<enitity> manager::add_enitity(std::string texture_name)
	{
		std::shared_ptr<enitity> x(new enitity(texture_name));
		Enities_Vectors.push_back(x);
		return x;
	}

	std::shared_ptr<enitity> manager::add_enitity(std::shared_ptr<enitity> & n_enitity)
	{
		Enities_Vectors.push_back(n_enitity);
		return n_enitity;
	}

	std::shared_ptr<enitity> manager::add_enitity(enitity n_enitity)
	{
		auto x = std::make_shared<enitity>(n_enitity);
		Enities_Vectors.push_back(x);
		return x;
	}

	std::shared_ptr<Window> manager::add_Window(sf::IntRect Rect, std::string Name)
	{
		std::shared_ptr<Window> x(new Window(Rect, Name));
		Windows_Vector.push_back(x);
		return x;
	}


	void manager::AddQuit(const std::function<int()> &nQuit) { Closed += nQuit; }
	void manager::AddEvents(const std::function<int(sf::Event)> &nEvent) { SFML_events += nEvent; }

	float manager::GetFrameTime()
	{
		//sf::Clock clock;
		sf::Time time;
		time = clock.getElapsedTime();
		return time.asSeconds();
	}

	float manager::ResetClock()
	{
		sf::Time time = clock.restart();
		return time.asSeconds();
	}

	std::shared_ptr<enitity> Player_Char;
	int animation_offset;
	int avatar_offset;


				
	int raw_event_funtion(sf::Event event)
	{
		float x, y;
		int y_offset = 0;
		x = y = 0.0f;
		switch (event.type)
		{

		case sf::Event::KeyPressed:
		case sf::Event::KeyReleased:
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) { y = -5.0f; y_offset = 3; }
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) { y = 5.0f; y_offset = 0; }
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) { x = -5.0f; y_offset = 1; }
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) { x = 5.0f; y_offset = 2; }
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::F1))
			{
				if (avatar_offset > 0)
					avatar_offset -= 3;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::F2))
			{
							
				avatar_offset += 3;
			}


						
			Player_Char->sprite.move(x, y);
			std::cout << "avatar state: " << avatar_offset << '\n';
			//std::cout << "x: " << x << "y: " << y << "animation_offset: " << animation_offset <<"Maths: " << (9 * animation_offset) << '\n';

			Player_Char->sprite.setTextureRect(sf::IntRect((32 * avatar_offset) + (32 *animation_offset) , 32 * y_offset, 32, 32));


			++animation_offset; if (animation_offset > 2) animation_offset = 0;



			break;
		default:
			//std::cout << event.type << '\n';

			break;
		}

					
		return 0;
	}
	int test_main()
	{



		animation_offset = 0;
		avatar_offset = 9;
		manager tm;
		auto MyWindow = tm.add_Window(sf::IntRect(16, 16, 256, 128), "purrmaid window!");
		tm.AddEvents(raw_event_funtion);
		tm.add_enitity("swifty3.png");
		Player_Char = tm.add_enitity("Eevees_ss3.png");
		std::cout << "[f1]/[f2] to change eeveelution\n";
		Player_Char->sprite.setTextureRect(sf::IntRect(32 * avatar_offset, 32 * 0, 32, 32));
		Player_Char->sprite.setPosition(256.0f, 256.0f);

		int bullshit = 0;

					
		while (tm.isOpen()) {
			//MyWindow->SetBoarderThickness(1);
			//if (bullshit >= 0xf) bullshit = 0;
			tm.do_render();
			tm.do_events();
		}

		return 0;
	}
	//int del = test_main();

}
