#pragma once
#include "PurrmaidRenderer.hpp"

#include "PurrmaidAI_State.hpp"
#include <iostream>
#ifdef SFML
	#include <SFML/Main.hpp>
	#include <SFML/System.hpp>
	#include <SFML/Window.hpp>
	#include <SFML/Graphics.hpp>
#endif
class Base_Object;
namespace Base_Object_State {
	int Idle(Base_Object& BO);
	int Decide_to_Move(Base_Object& BO);
	int Moving_To_Target(Base_Object& BO);
	int At_Moving_Target(Base_Object& BO);
	int Default_State(Base_Object& BO);
	int Unknown_State(Base_Object& BO);
}

class Base_Object : public Purrmaid::renderer::sfml::enitity
{
public:
	Base_Object(std::string);
	int getState();
	void setState(const int & n_state);

	sf::Vector2f getTargetLocation();
	void setTargetLocation(float x, float y);

	void do_ai();

private:

	Purrmaid::State_CLS <Purrmaid::State_Pair<decltype(Base_Object_State::Default_State)*, int>, 6> _State;
	int _current_state;
	//StateData _State;

	sf::Vector2f TargetLocation;
};