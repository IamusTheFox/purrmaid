#pragma once
//#include <SDL.h>
//#include <SDL_mixer.h>
#include <thread>
#include <future>
#include <memory>
#include <mutex>

#include "ThreadManager.hpp"

#include "renderer_data.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "PurrmaidDelegate.hpp"
#include "PurrmaidScripting.hpp"
#include "PurrmaidRenderer.hpp"

#include <vector>
#include <functional>
#include <tuple>
namespace Purrmaid::purrmaid::unit_tests
{
	class tester
	{
	protected:
		std::vector < std::tuple< std::function<bool() >, std::string>> Test_Fucntion_list;
		std::vector <bool> failed_list;
		bool has_failed;
		
		std::string on_failed(std::tuple<std::function<bool() >, std::string>);
	public:
		tester();
		void add_function(std::function<bool()> nFunt, std::string on_fail_message);
		void do_tests();
	};
}