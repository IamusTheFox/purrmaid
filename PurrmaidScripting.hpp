//#pragma once
#include <string>

#include "PurrmaidDelegate.hpp"

/*
#ifdef _Win32
	#include <lua.hpp>
#elif _unix_
	#include <lua53/lua.hpp>
#endif
*/

#include <lua.hpp>
#include <stdio.h>

#include <iostream>

#include <functional>
#include <vector>

namespace Purrmaid
{
	namespace Scripting
	{
		namespace Lua
		{
			//using function_t = int(lua_State*);

			struct Function_Data
			{
				std::string Function_Name;
				std::function<int(lua_State*)> Function;
			};
			class Manager
			{
			protected:
				lua_State* current_state;
				std::string ScriptName;

				std::vector<Function_Data> Functions;
			public:
				Manager(std::string Name);

				bool load_file(std::string SName);

				bool run_script();

				void add_script_funtion(std::string, std::function<int(lua_State*)>);


			};
		}
	}
}
