
#include <stdio.h>
#include <iostream>
#include <vector>
#include "PurrmaidScripting.hpp"

//#include <lua53/lua.hpp>
namespace Purrmaid
{
	namespace Scripting
	{
		namespace Lua
		{

			bool Manager::load_file(std::string SName)
			{
				ScriptName = SName;
				return true;
			}


			bool Manager::run_script()
			{

				int result = luaL_loadfile(current_state, ScriptName.c_str());

				if (result != LUA_OK) {

					return true;
				}

				result = lua_pcall(current_state, 0, LUA_MULTRET, 0);
				lua_close(current_state);

				if (result != LUA_OK) {
					return false;
				}
				return true;


			}

			void Manager::add_script_funtion(std::string s, std::function<int(lua_State*)> f)
			{
				lua_register(current_state, s.c_str(), f.target<int(lua_State*)>());


			}
			Manager::Manager(std::string Name)
			{
				current_state = luaL_newstate();

				luaL_openlibs(current_state);

				ScriptName = Name;
			}
		}
	}

}