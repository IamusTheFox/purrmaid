#include "Purrmaid.hpp"
#include "PurmaidPrint.hpp"
#include "PurrmaidRenderer.hpp"


namespace Purrmaid::purrmaid::unit_tests
{
	
	std::string tester::on_failed(std::tuple<std::function<bool()>, std::string> tp)
	{
		return std::get<1>(tp);
	}
	tester::tester() : Test_Fucntion_list{}, failed_list{}, has_failed{false}
	{}
	void tester::add_function(std::function<bool()> nFunt, std::string message)
	{
		Test_Fucntion_list.push_back({ nFunt, message });
	}

	void tester::do_tests()
	{
		for (auto current_tuple : Test_Fucntion_list)
		{
			std::function<bool()> func = std::get<0>(current_tuple);

			auto result = func();
			failed_list.push_back(result);
			if (result == false)
				has_failed = true;
		}
	}
	
}